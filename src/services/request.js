import _axios from 'axios';

import { BASE_URL, TIMEOUT} from './config';

const axios = _axios.create({
    baseURL:BASE_URL,
    timeout:TIMEOUT
})

//添加攔截
axios.interceptors.request.use(config => {
    return config
},error => {

})

axios.interceptors.response.use(res => {
    return res.data
},error => {
    return error;
})




export default axios;
