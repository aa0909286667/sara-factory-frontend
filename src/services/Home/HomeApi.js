import axios from "../request"

const overviewData = '/api/get_project';
const jobList = '/api/get_job_list';
const resources = '/api/manage_resources';
const dashboardInfo = '/api/get_dashboard_info/jlb_name_query_form';
const calendar = '/api/get_calendar';
const hours = '/api/get_shift';
const addResource = '/api/get_resource'
const addProduct = '/api/get_product'
const gantt = '/api/load_gantt'

export default {

    async getOverviewData() {
        const result = await axios.get(overviewData);
        return result;
    },

    async getOverviewHold( val ) {
        const result = await axios.put(overviewData + '/' + val,{
            status: "hold",
        });
        return result;
    },

    async getOverviewResume( val ) {
        const result = await axios.put(overviewData + '/' + val,{
            status: "resume",
        });
        return result;
    },

    async getDeleteOverview( val ) {
        const result = await axios.delete(overviewData + '/' + val);
        return result;
    },

    async getJobList() {
        const result = await axios.get(jobList);
        return result;
    },

    async getResources() {
        const result = await axios.get(resources);
        return result;
    },

    async getDashboardInfo() {
        const result = await axios.post(dashboardInfo);
        return result;
    },

    async getCalendar() {
        const result = await axios.get(calendar);
        return result;
    },

    async getHours() {
        const result = await axios.get(hours);
        return result;
    },

    async addResource(val) {
        const result = await axios.post(addResource,{
                resource_type: val.resource_type,
                resource_name: val.resource_name,
                job_name: val.job_name.toString(),
                allow_overlapped: "1",
                calendar_name: val.calendar_name,
                hours_name: val.hours_name

        });
        return result;
    },

    async addWorkOrder(val) {
        const result = await axios.post(overviewData,{
            order_id: val.order_id,
            order_date: val.order_date,
            project_name: val.project_name,
            project_type: val.project_type,
            due: val.due,

        });
        return result;
    },

    async getProduct(val) {
        const result = await axios.post(addProduct,{
            product_name: val.product_name,
            lot_nbr: val.lot_nbr,
            project_id: val.project_id
        });
        return result;
    },

    async getGantt() {
        const result = await axios.post(gantt);
        return result;
    },





}
