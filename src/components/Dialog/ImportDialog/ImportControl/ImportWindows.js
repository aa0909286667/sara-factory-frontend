import React from "react";
import {FormattedMessage, injectIntl} from "react-intl";
import { ImportDialog } from "../index";


const ImportWindowsTemplate = props =>{

    const { ImportDialog } = props;

    return(
        <div className="center">
            <h3 className="f-25 margin-top-bottom-40">
                <FormattedMessage
                    id="no_data.no_data"
                    defaultMessage="No Data To Display."
                />
            </h3>
            <h4 className="f-16 margin-top-bottom-40">
                <FormattedMessage
                    id="no_data.no_data_teaching"
                    defaultMessage="You can do the following operations."
                />
            </h4>
            <ImportDialog className="margin-top-bottom-40" />
        </div>
    )
}

const ImportWindows = injectIntl( ImportWindowsTemplate );
export default ImportWindows;
