import React, { Component } from 'react';


class Login extends Component {
    render() {
        return (
            <div className="auth-wrapper">
                <div className="auth-content">
                    <div className="card">
                        <div className="card-body text-center">
                            <p>Test</p>
                        </div>
                        <div className="card-footer text-center">
                            <span className="text-white f-16">© 2021 InterAgent.io</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export { Login };
