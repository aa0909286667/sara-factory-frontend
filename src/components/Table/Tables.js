// 基本
import React from "react";
import { FormattedMessage, injectIntl } from "react-intl";
// Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import ImportWindows from "../Dialog/ImportDialog/ImportControl/ImportWindows";
// Button
import Button from "@material-ui/core/Button";


const TablesTemplate = props => {

    // 父頁面值
    const { columns, rows, formatMessage, tableType, noData } = props;


    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    function ShowTable() {
        if (rows.length === 0 && noData !== undefined) {
            return(
                <TableBody>
                    <TableRow>
                        <th className="no_data" colSpan="8">
                            <ImportWindows  ImportDialog={ noData } />
                        </th>
                    </TableRow>
                </TableBody>
            );
        } else if(rows.length === 0 && noData === undefined) {

        }
        return(
            rows.map((row, index) => (
                <TableBody key={ index }>
                    <TableRow>
                        { columns.map(( column, columnsIndex ) => {
                            return <TableBodyTd key={ columnsIndex } row={ row } index={ columnsIndex } column={ column } />
                        })}
                    </TableRow>
                </TableBody>
            ))
        ) ;
    };

    const TableBodyTd = props => {
        const {row, index, column} = props

        const value = row[column.id];


        if ( typeof(value) === "string" ) {
            return(
                <TableCell key={ value }>
                    { value }
                </TableCell>
            )
        } else if( typeof(value) === "function" && value !== null){
            const Element = value
            return(
                <TableCell key={ value }>
                    <Element row={ row } index={ index } column={ column } formatMessage={ formatMessage } />
                </TableCell>
            )
        } else if( typeof(value) === "function" && value !== null ) {
            const Element = value
            return (
                <TableCell key={ value }>
                    <Element row={ row } index={ index } column={ column } formatMessage={ formatMessage } />
                </TableCell>
            )
        }else {
            return(
                <TableCell key={ value } />
            )
        }


    }


    return (
        <Paper>
            <TableContainer style={{  maxHeight: "375px", height: "375px" }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (

                                <TableCell
                                    className="center"
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <ShowTable />
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={ [10, 25, 100] }
                component="div"
                count={rows.length}
                rowsPerPage={ rowsPerPage }
                page={ page }
                onChangePage={ handleChangePage }
                onChangeRowsPerPage={ handleChangeRowsPerPage }
            />
        </Paper>
    );
}

const Tables = injectIntl( TablesTemplate );
export default Tables;
