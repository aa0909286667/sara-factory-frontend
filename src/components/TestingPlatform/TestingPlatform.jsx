// 基本
import React from "react";
import { injectIntl, FormattedMessage } from 'react-intl';
import { useTheme } from '@material-ui/core/styles';
// Card
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import AppBar from "@material-ui/core/AppBar";
import CardActions from '@material-ui/core/CardActions';
// Import Data Dialog
import ImportDataDialog from "../Dialog/ImportDialog/ImportControl/ImportDataDialog";
import ImportWindows from "../Dialog/ImportDialog/ImportControl/ImportWindows";
// Step
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
// MobileStepper
import MobileStepper from '@material-ui/core/MobileStepper';
// icon
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
// auto play
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

// Tabs
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

function getSteps() {
    return [
        "匯入資料",
        "重排",
        "發行",
    ];
}

// 匯出pdf圖片
const tutorialSteps = [
    {
        imgPath:
            'https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        imgPath:
            'https://images.unsplash.com/photo-1538032746644-0212e812a9e7?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        imgPath:
            'https://images.unsplash.com/photo-1537996194471-e657df975ab4?auto=format&fit=crop&w=400&h=250&q=80',
    },
    {
        imgPath:
            'https://images.unsplash.com/photo-1518732714860-b62714ce0c59?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        imgPath:
            'https://images.unsplash.com/photo-1512341689857-198e7e2f3ca8?auto=format&fit=crop&w=400&h=250&q=60',
    },
];


class TestingPlatformTemplate extends React.Component {
    render(){
        const { formatMessage } = this.props.intl;

        const TestingPlatformWindows = () => {

            // Step 基本參數
            const [activeStep, setActiveStep] = React.useState(0);
            const steps = getSteps();
            const [skipped, setSkipped] = React.useState(new Set());

            const handleNext = (steps) => {
                setActiveStep((prevActiveStep) => steps);
            };

            const handleBack = () => {
                setActiveStep((prevActiveStep) => prevActiveStep - 1);
            };

            const handleReset = () => {
                setActiveStep(0);
            };

            // image options
            const [imageOptionsStep, setImageOptionsStep] = React.useState(0);

            const imageOptionsNext = () => {
                setImageOptionsStep((prevActiveStep) => prevActiveStep + 1);
            };

            const imageOptionsBack = () => {
                setImageOptionsStep((prevActiveStep) => prevActiveStep - 1);
            };

            const imageOptionsChange = (step) => {
                setImageOptionsNum(step);
            };

            const theme = useTheme();

            // image options Number

            const [imageOptionsNum, setImageOptionsNum] = React.useState(tutorialSteps.length);

            const imageOptionsNumAdd = (imageNum) => {
                setImageOptionsNum((prevActiveStep) => prevActiveStep + 1);
            };

            // 左邊圖案
            function LeftImg() {
                if( imageOptionsStep === 0){
                    return(
                        <img className="mini-img" src={tutorialSteps[imageOptionsNum - 1].imgPath}></img>
                    )
                }else {
                    return(
                        <img className="mini-img" src={tutorialSteps[imageOptionsStep - 1].imgPath}></img>
                    )
                }
            }
            // 右邊圖案
            function RightImg() {
                if(imageOptionsStep + 1 === imageOptionsNum){
                    return(
                        <img className="mini-img" src={tutorialSteps[0].imgPath}></img>
                    )
                } else {
                    return(
                        <img className="mini-img" src={tutorialSteps[imageOptionsStep + 1].imgPath}></img>
                    )
                }
            }

            // Tabs
            function a11yProps() {
                return {
                    id: `full-width-tab-${imageOptionsStep}`,
                    'aria-controls': `full-width-tabpanel-${imageOptionsStep}`,
                };
            }

            function TabPanel(props) {
                const { children, value, index, ...other } = props;

                return (
                    <div
                        role="tabpanel"
                        hidden={value !== index}
                        id={`full-width-tabpanel-${index}`}
                        aria-labelledby={`full-width-tab-${index}`}
                        {...other}
                    >
                        {value === index && (
                            <Box p={3}>
                                <Typography component="span">{ children }</Typography>
                            </Box>
                        )}
                    </div>
                );
            }


            return(
                <div className="testing-platform">
                    <Card className="card">
                        <CardHeader
                            title={
                                <h3 className="f-30">
                                    <FormattedMessage
                                        id="testing-platform-title"
                                        defaultMessage="Testing Platform"
                                    />
                                </h3>
                            }
                        />
                        <Stepper activeStep={ activeStep }>
                            { steps.map((label, index) =>{
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step key={label} {...stepProps}>
                                        <StepLabel {...labelProps}>{label}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <CardContent>
                            <SwipeableViews
                                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                index={ imageOptionsStep }
                            >
                                <TabPanel value={ imageOptionsStep } index={0} dir={theme.direction}>
                                    <AppBar position="static" color="default">
                                        <ImportWindows ImportDialog={ ImportDialog } />
                                    </AppBar>
                                </TabPanel>
                                <TabPanel value={ imageOptionsStep } index={1} dir={theme.direction}>
                                    Item Two
                                </TabPanel>
                                <TabPanel value={ imageOptionsStep } index={2} dir={theme.direction}>
                                    Item Three
                                </TabPanel>
                            </SwipeableViews>

                        </CardContent>
                        <CardContent className="center padding-0" style={{ padding: 0 }}>
                            <LeftImg />
                            <img className="center-imag" src={tutorialSteps[imageOptionsStep].imgPath}></img>
                            <RightImg />
                        </CardContent>
                        <CardContent className="center padding-0">
                            <MobileStepper
                                steps={ imageOptionsNum }
                                position="static"
                                activeStep={ imageOptionsStep }
                                nextButton={
                                    <Button
                                        size="small"
                                        onClick={ imageOptionsNext }
                                        disabled={ imageOptionsStep === imageOptionsNum - 1 || imageOptionsNum === 0}
                                    >
                                        Next
                                        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                                    </Button>
                                }
                                backButton={
                                    <Button
                                        size="small" onClick={ imageOptionsBack }
                                        disabled={ imageOptionsStep === 0}
                                    >
                                        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                        Back
                                    </Button>
                                }
                            >
                                <div className="MuiMobileStepper-dot">Test</div>
                            </MobileStepper>
                        </CardContent>
                    </Card>
                </div>
            )
        }

        return (<TestingPlatformWindows />)
    }
}
const TestingPlatform = injectIntl( TestingPlatformTemplate );
export { TestingPlatform }
