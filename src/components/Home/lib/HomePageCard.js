// 基本
import React from "react";
import { useTheme } from '@material-ui/core/styles';
import { injectIntl } from "react-intl";
// Card
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
// Prop
import PropTypes from 'prop-types';
// App Bar
import AppBar from '@material-ui/core/AppBar';
// Tab
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div>
            {value === index && (
                <Box p={ 3 }>
                    <Typography component="span">{ children }</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const HomePageCardTemplate = props =>{

    const { HomePageCardItems, value, handleChange} = props;


    const theme = useTheme();


    return(
        <div>
            <Card className="card">
                <CardHeader
                    title={
                        <h3 className="f-30">
                            { HomePageCardItems.title }
                        </h3>
                    }
                />
                <CardContent>
                    <AppBar position="static" color="default">
                        <Tabs
                            value={ value }
                            onChange={ handleChange }
                            indicatorColor="primary"
                            textColor="primary"
                            variant="fullWidth"
                            aria-label="full width tabs example"
                        >
                            { HomePageCardItems.pageItems.map((page,index) => (
                                <Tab
                                    key={ index }
                                    label={ page.pageTitle }
                                    {...a11yProps(index)}
                                />
                            ))}

                        </Tabs>
                    </AppBar>
                    { HomePageCardItems.pageItems.map((page,index) => (
                        <TabPanel key={ index } value={ value } index={ index } dir={ theme.direction }>
                            { page.pageContent }
                        </TabPanel>
                    ))}
                </CardContent>
            </Card>
        </div>
    )
}

const HomePageCard = injectIntl( HomePageCardTemplate );
export default HomePageCard;
