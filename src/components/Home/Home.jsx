// 基本
import React from "react";
import {injectIntl, FormattedMessage} from 'react-intl';
// Tab
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
// Tabs 元件
import { WorkOrder } from './ component/work-order'
import { Process } from './ component/process'
import { Resources } from './ component/resources'


function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography component="span" >{ children }</Typography>
                </Box>
            )}
        </div>
    );
}
function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}
function BasicTabs( formatMessage ) {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <Box sx={{ width: '100%' }}>
            <TabPanel value={value} index={0}>
                <WorkOrder />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Process />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <Resources />
            </TabPanel>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab
                        style={ {color: "wheat"} }
                        label={
                            <FormattedMessage
                                id="home.work_order"
                                defaultMessage="Work Order"/>
                        }
                        {...a11yProps(0)} />
                    <Tab
                        style={ {color: "wheat"} }
                        label={
                            <FormattedMessage
                                id="home.process"
                                defaultMessage="Process"/>
                        }
                        {...a11yProps(1)} />
                    <Tab
                        style={ {color: "wheat"} }
                        label={
                            <FormattedMessage
                                id="home.resources"
                                defaultMessage="Cancel"/>
                        }
                        {...a11yProps(2)} />
                </Tabs>
            </Box>
        </Box>
    );
}
class HomeTemplate extends React.Component {
    render(){
        const { formatMessage } = this.props.intl;
        return(
            <div>
                <BasicTabs formatMessage={ formatMessage } />
            </div>
        )
    }
}
const Home = injectIntl( HomeTemplate );
export { Home }
