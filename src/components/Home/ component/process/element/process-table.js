import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";
// Api
import ApiService from "../../../../../services/Home/HomeApi";
// Swal
import Swal from 'sweetalert2'

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

// Table Key
function createData(job_name, process_name, sourcing, resource_type, status) {
    return { job_name, process_name, sourcing, resource_type, status };
}

// Table Items
const rows = [];

// 取得製程管理Api
ApiService.getJobList().then(
    (response) => {
        const job_list_name = Object.keys(response)
        job_list_name.forEach((name) => {
            rows.push(
                createData(
                    response[name][0],
                    response[name][1],
                    response[name][2],
                    getResourceType(response[name][3]),
                    getStatus(response[name][3]),
                )
            )
        })
    }
);

function getResourceType(val) {

    const resourceType = Object.keys(val);

    const resourceTypeName = []

    resourceType.forEach((name) => {
        resourceTypeName.push('[ '+name+' / '+ val[name].length +' ]')
    })

    return resourceTypeName.toString().replace(/,/g, "、");

}

function getStatus(val) {

    if( Object.keys(val).indexOf('virtual') ) {
        return "可使用"
    } else {
        return "建立中"
    }

}



const ProcessTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;


    // Table Header
    const columns = [
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"process.job_name",
                    defaultMessage: "Order ID"
                }),
        },
        {
            id: 'process_name',
            label:
                formatMessage({
                    id:"process.process_name",
                    defaultMessage: "Process Name"
                }),
        },
        {
            id: 'sourcing',
            label:
                formatMessage({
                    id:"process.sourcing",
                    defaultMessage: "Sourcing"
                }),
        },
        {
            id: 'resource_type',
            label:
                formatMessage({
                    id:"process.resource_type",
                    defaultMessage: "Resource Type"
                }),
        },
        {
            id: 'status',
            label:
                formatMessage({
                    id:"process.status",
                    defaultMessage: "Status"
                }),
        },
    ];


    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const ProcessTable = injectIntl( ProcessTableTemplate );
export default ProcessTable;
