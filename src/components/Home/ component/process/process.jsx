// 基本
import React from "react"
import { injectIntl, FormattedMessage } from 'react-intl';
// Card
import HomePageCard from "../../lib/HomePageCard";
// Table
import ProcessTable from "./element/process-table";

class ProcessTemplate extends React.Component {

    render(){
        // i18n
        const { formatMessage } = this.props.intl;


        function PageCard() {

            const [value, setValue] = React.useState(0);

            const handleChange = (event, newValue) => {
                setValue(newValue);
            };

            const HomePageCardItems ={
                formatMessage: formatMessage,
                title: formatMessage({
                    id: "home.process",
                    defaultMessage: "Process"
                }),
                pageItems:[
                    {
                        pageTitle: formatMessage({
                            id: "home.process_table",
                            defaultMessage: "Process Table"
                        }),
                        pageContent: <ProcessTable formatMessage={formatMessage} />,

                    },
                ]
            }

            return(
                <HomePageCard
                    value={ value }
                    handleChange={ handleChange }
                    HomePageCardItems={ HomePageCardItems }
                />
            )
        }

        return(
            <div className="process">
                <PageCard />
            </div>
        )
    }
}

const Process = injectIntl( ProcessTemplate );

export { Process }
