import React from "react";
import Tables from "../../../../Table/Tables";
import {FormattedMessage, injectIntl} from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";
// Button
import Button from "@material-ui/core/Button";
// Api
import ApiService from "../../../../../services/Home/HomeApi";
// Swal
import Swal from 'sweetalert2'

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

// Table Key
function createData(order_id, project_name, part_number, project_type, order_date,due,plan_end_time,status, button,pid) {
    return { order_id, project_name, part_number, project_type, order_date,due,plan_end_time,status, button,pid };
}

const OverviewBtn = props => {
    const {row, index, column, formatMessage} = props



    function hold() {
        ApiService.getOverviewHold( row.pid ).then(
            (response) => {
                window.location.reload()
            })
    }
    function resume() {
        ApiService.getOverviewResume( row.pid ).then(
            (response) => {
                window.location.reload()
            })
    }
    function deleteOverview() {
        Swal.fire({
            title: formatMessage({
                id:"Swal.delete_confirm",
                defaultMessage: "Are You Sure Want To Delete？"
            }),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#28a745',
            confirmButtonText: formatMessage({
                id:"Swal.delete",
                defaultMessage: "Delete"
            }),
            cancelButtonText: formatMessage({
                id:"Swal.cancel",
                defaultMessage: "Cancel"
            }),
        }).then((result) => {
            if (result.isConfirmed) {
                ApiService.getDeleteOverview( row.pid ).then(
                    (response) => {
                        Swal.fire({
                            icon: 'success',
                            title: formatMessage({
                                id:"Swal.success",
                                defaultMessage: "Success"
                            }),
                            showConfirmButton: false,
                            timer: 2000
                        })
                        window.location.reload()
                    })
            }
        })
    }

    function ControlBtn () {
        if(row.status === 'hold'){
            return(
                <Button
                    className="margin-3"
                    variant="contained"
                    color="primary"
                    onClick={ resume }
                >
                    <FormattedMessage
                        id="work-order.resume"
                        defaultMessage="Resume"/>
                </Button>
            )
        } else {
            return (
                <Button
                    className="margin-3 btn-oval-success"
                    variant="contained"
                    color="secondary"
                    onClick={ hold }
                >
                    <FormattedMessage
                        id="work-order.hold"
                        defaultMessage="Hold"/>
                </Button>
            )
        }
    }
    return(
        <div>
            <ControlBtn />
            <Button
                className="margin-3 btn-oval-success"
                variant="contained"
                onClick={ deleteOverview }
            >
                <FormattedMessage
                    id="work-order.delete"
                    defaultMessage="Delete"/>
            </Button>
        </div>
    )
}

// Table Items
let rows = [];


// 取得工作訂單Api
ApiService.getOverviewData().then(
    (response) => {
        console.log(response)
        const PID = Object.keys(response)
        PID.forEach((pid, index) => {
            rows.push(
                createData(
                    response[pid].order_id,
                    response[pid].project_name,
                    response[pid].project_name,
                    response[pid].project_type,
                    response[pid].order_date,
                    response[pid].due,
                    response[pid].plan_end_time,
                    response[pid].status,
                    OverviewBtn,
                    pid
                )
            )
        })
    }
);


const WorkOrderTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'order_id',
            label:
                formatMessage({
                    id:"work-order.order_id",
                    defaultMessage: "Order ID"
                }),
        },
        {
            id: 'project_id',
            label:
                formatMessage({
                    id:"work-order.project_id",
                    defaultMessage: "Project ID"
                }),
        },
        {
            id: 'part_number',
            label:
                formatMessage({
                    id:"work-order.part_number",
                    defaultMessage: "Part Number"
                }),
        },
        {
            id: 'project_type',
            label:
                formatMessage({
                    id:"work-order.project_type",
                    defaultMessage: "Project Type"
                }),
        },
        {
            id: 'order_date',
            label:
                formatMessage({
                    id:"work-order.order_date",
                    defaultMessage: "Order Date"
                }),
        },
        {
            id: 'due',
            label:
                formatMessage({
                    id:"work-order.due",
                    defaultMessage: "Due"
                }),
        },
        {
            id: 'plan_end_time',
            label:
                formatMessage({
                    id:"work-order.plan_end_time",
                    defaultMessage: "Plan End Time"
                }),
        },
        {
            id: 'status',
            label:
                formatMessage({
                    id:"work-order.status",
                    defaultMessage: "Status"
                }),
        },
        {
            id: 'button',
            label:
                formatMessage({
                    id:"work-order.action",
                    defaultMessage: "Action"
                }),
        },
    ];







    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            noData={ ImportDialog }
        />
    )
}

const WorkOrderTable = injectIntl( WorkOrderTableTemplate );
export default WorkOrderTable;
