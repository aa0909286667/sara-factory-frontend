// 基本
import React, { useState, useEffect } from "react";
import { injectIntl } from "react-intl";

// 流程ui
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// Table
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// TextField
import TextField from '@material-ui/core/TextField';
// Api
import ApiService from "../../../../../services/Home/HomeApi";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
// Table
import Table from '@material-ui/core/Table';

let dashboardInfoObj = {}

ApiService.getDashboardInfo().then(
    (response) => {
        console.log(response)
        dashboardInfoObj = response
    }
);


const AddWorkOrderTemplate = props => {
    // 父頁面值
    const { formatMessage, setValue } = props;

    // 流程開關
    const [activeStep, setActiveStep] = React.useState(0);

    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    // 控制 select 開關
    const [open, setOpen] = React.useState(false);


    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const [jlbNameOpen, setJlbNameOpen] = React.useState(false);


    const handleJlbNameClose = () => {
        setJlbNameOpen(false);
    };

    const handleJlbNameOpen = () => {
        setJlbNameOpen(true);
    };

    // 工作單
    const [addWorkOrder, setAddWorkOrder] = useState({
        order_id: '', // 訂單編號
        order_date: new Date().toISOString().substr(0, 10), // 預設date
        project_name: '', // 工單編號
        project_type: '一般', //類別
        due: new Date().toISOString().substr(0, 10), //工單交期
    })

    // 取得PID 用於刪除工作單 及 新增料號
    const [pid, getPid] = useState('')

    // 監控取得PID 並給值給 料號 及 新增工序使用
    useEffect(() => {
        setAddProduct({...addProduct,project_id: pid });
        setAddProcess({...addProcess,project_id: pid });
    },[pid])


    // 料號新增行列
    const [productRowNum, setProductRowNum] = React.useState(1);

    const handleProductRowNum = () => {
        setProductRowNum((prevActiveStep) => prevActiveStep + 1);
    };

    // 料號
    const [addProduct, setAddProduct] = React.useState({
        product_name: '', // 料號
        lot_nbr: '', // 批號
        project_id: '', // 新增工單後的pid
    });


    // 站台
    const [dashboardInfo, setDashboardInfo] = React.useState(dashboardInfoObj);

    let process_name_list = []

    Object.keys(dashboardInfo).forEach((items) => {

        process_name_list.push(dashboardInfo[items].process_name)

        process_name_list = process_name_list.filter(function(element, index, arr){
            return arr.indexOf(element) === index;
        });

    })

    //製程
    const [jlbName, setJlbName] = React.useState([]);

    // 新增工序列表
    const [addProcess, setAddProcess] = React.useState({
        job_sequence: 1, // 製品流程
        process_name: process_name_list[0], // 站點名稱
        jlb_name: '', //製成名稱
        qty: 1, // 生產數量
        sourcing: '', // 內制外包
        est_time: 1,// 預估工時
        job_name: '',
        project_id: '',
    });

    // 製品流程序列號
    const [date, setDate] = React.useState("minute");

    const dateChange = (event) => {
        setDate(event.target.value);
    };

    const [openDate, setOpenDate] = React.useState(false);

    const dateSelectClose = () => {
        setOpenDate(false);
    };

    const dateSelectOpen = () => {
        setOpenDate(true);
    };


    function getSteps() {
        return ['建立工單', "新增料號", "新增工序"];
    }


    function getStepContent(step) {
        switch (step) {
            case 0:
                return (
                    <div className="margin-20">
                        <TableContainer >
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>訂單編號</TableCell>
                                        <TableCell>工單編號</TableCell>
                                        <TableCell>類別</TableCell>
                                        <TableCell>工單交期</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                multiline
                                                rowsMax={4}
                                                value={ addWorkOrder.order_id }
                                                onChange={(evt) => {
                                                    setAddWorkOrder({...addWorkOrder,order_id: evt.target.value });
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                multiline
                                                value={ addWorkOrder.project_name }
                                                onChange={(evt) => {
                                                    setAddWorkOrder({...addWorkOrder,project_name: evt.target.value });
                                                }}
                                                rowsMax={4}
                                                style={{  minWidth: 180,width: 180 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <Select
                                                open={ open }
                                                value={ addWorkOrder.project_type }
                                                onClose={ handleClose }
                                                onOpen={ handleOpen }
                                                onChange={(evt) => {
                                                    setAddWorkOrder({...addWorkOrder,project_type: evt.target.value });
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            >
                                                <MenuItem value={ "一般" }>一般</MenuItem>
                                                <MenuItem value={ "急件" }>急件</MenuItem>
                                            </Select>
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="date"
                                                type="date"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                value={ addWorkOrder.due }
                                                onChange={(evt) => {
                                                    setAddWorkOrder({...addWorkOrder,due: evt.target.value });
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                )
            case 1:
                return (
                    <div className="margin-20" >
                        <TableContainer >
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>料號</TableCell>
                                        <TableCell>批號</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                multiline
                                                rowsMax={4}
                                                value={ addProduct.product_name }
                                                onChange={(evt) => {
                                                    setAddProduct({...addProduct,product_name: evt.target.value });
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                multiline
                                                rowsMax={4}
                                                value={ addProduct.lot_nbr }
                                                onChange={(evt) => {
                                                    setAddProduct({...addProduct,lot_nbr: evt.target.value });
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                )

            case 2:
                return (
                    <div className="margin-20">
                        <TableContainer>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>製品流程</TableCell>
                                        <TableCell>站點名稱</TableCell>
                                        <TableCell>製程名稱</TableCell>
                                        <TableCell>生產數量</TableCell>
                                        <TableCell>內制/外包</TableCell>
                                        <TableCell>
                                            預估工時(
                                            <Select
                                                className="date-select"
                                                labelId="demo-controlled-open-select-label"
                                                id="demo-controlled-open-select"
                                                open={ openDate }
                                                onClose={ dateSelectClose }
                                                onOpen={ dateSelectOpen }
                                                value={ date }
                                                onChange={ dateChange }
                                            >
                                                <MenuItem value={ "minute" }>分</MenuItem>
                                                <MenuItem value={ "hour" }>時</MenuItem>
                                                <MenuItem value={ "day" }>天</MenuItem>
                                            </Select>
                                            )
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                disabled
                                                multiline
                                                rowsMax={4}
                                                value={ addProcess.job_sequence }
                                                onChange={(evt) => {
                                                    setAddProcess({...addProcess,job_sequence: evt.target.value });
                                                }}
                                                style={{  minWidth: 120,width: 120 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <Select
                                                open={ open }
                                                onClose={ handleClose }
                                                onOpen={ handleOpen }
                                                value={ addProcess.process_name }
                                                onChange={(evt) => {
                                                    getJlbName(evt)

                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            >
                                                {
                                                    process_name_list.map((processName) => {
                                                        return(
                                                            <MenuItem key={ processName } value={ processName }>{processName}</MenuItem>
                                                        )
                                                    })
                                                }
                                            </Select>
                                        </TableCell>
                                        <TableCell>
                                            <Select
                                                open={ jlbNameOpen }
                                                onClose={ handleJlbNameClose }
                                                onOpen={ handleJlbNameOpen }

                                                value={ addProcess.jlb_name }
                                                onChange={(evt) => {
                                                    getSourcing(evt)
                                                }}
                                                style={{  minWidth: 180,width: 180 }}
                                            >
                                                {
                                                    jlbName.map((jlbName) => {
                                                        return(
                                                            <MenuItem key={ jlbName } value={ jlbName }>{ jlbName }</MenuItem>
                                                        )
                                                    })
                                                }
                                            </Select>
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="number"
                                                type="number"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                value={ addProcess.qty }
                                                onChange={(evt) => {
                                                    setAddProcess({...addProcess,qty: evt.target.value });
                                                }}
                                                style={{  minWidth: 120,width: 120 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="outlined-multiline-flexible"
                                                disabled
                                                multiline
                                                rowsMax={4}
                                                value={ addProcess.sourcing }
                                                onChange={(evt) => {
                                                    setAddProcess({...addProcess,sourcing: evt.target.value });
                                                }}
                                                style={{  minWidth: 120,width: 120 }}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <TextField
                                                id="number"
                                                type="number"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                value={ addProcess.est_time }
                                                onChange={(evt) => {
                                                    setAddProcess({...addProcess,est_time: evt.target.value });
                                                }}
                                                style={{  minWidth: 120,width: 120 }}
                                            />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                )
            default:
                return 'Unknown step';
        }
    }

    function getJlbName(evt) {
        let jlbNameList = []
        setAddProcess({...addProcess,process_name: evt.target.value });
        Object.keys(dashboardInfo).forEach((items) => {
            if( dashboardInfo[items].process_name === evt.target.value ) {
                jlbNameList.push(dashboardInfo[items].jlb_name)
            }
        })

        setJlbName(jlbNameList)
    }

    function getSourcing(evt) {
        setAddProcess({...addProcess,jlb_name: evt.target.value });
        setAddProcess({...addProcess,sourcing: dashboardInfo[evt.target.value].sourcing });
    }


    function submit() {
        if( activeStep === 0 ) {
            ApiService.addWorkOrder(addWorkOrder).then(
                (response) => {
                    getPid(response.data.index)
                    handleNext()
                }
            );
        } else if( activeStep === 1 ) {
            ApiService.getProduct(addProduct).then(
                (response) => {
                    handleNext()
                }
            );
        } else if( activeStep === 2 ) {
            handleNext()
        }
    }

    function stepBack(  ) {
        if( activeStep === 1 ) {
            ApiService.getDeleteOverview( pid ).then(
                (response) => {
                    handleBack()
                })
        } else if( activeStep === 2 ) {
            handleBack()
        }
    }

    return(
        <div style={{ maxHeight: "426px",  height: "426px" }} className="overflow-y-auto add-word-order">
            <Stepper activeStep={activeStep} orientation="vertical">
                {steps.map((label, index) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                        <StepContent>
                            <Typography component="span" >{getStepContent(index)}</Typography>
                            <div>
                                <div>
                                    <Button
                                        className="margin-20"
                                        color="secondary"
                                        variant="contained"
                                        disabled={activeStep === 0}
                                        onClick={ stepBack }
                                    >
                                        Back
                                    </Button>
                                    <Button
                                        className="margin-20"
                                        disabled={
                                            (activeStep === 0 && (addWorkOrder.order_id === "" || addWorkOrder.project_name === "")) ||
                                            (activeStep === 1 && (addProduct.product_name === "" || addProduct.project_id === ""))
                                        }
                                        variant="contained"
                                        color="primary"
                                        onClick={ submit }
                                    >
                                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                    </Button>
                                </div>
                            </div>
                        </StepContent>
                    </Step>
                ))}
            </Stepper>
            {activeStep === steps.length && (
                <Paper square elevation={0}>
                    <Typography className="margin-20" component="span">所有步骤完成</Typography>
                    <Button
                        color="primary"
                        variant="contained"
                        className="margin-20"
                        onClick={ handleReset }>
                        送出
                    </Button>
                </Paper>
            )}
        </div>

    )
}

const AddWorkOrder = injectIntl( AddWorkOrderTemplate );
export default AddWorkOrder;
