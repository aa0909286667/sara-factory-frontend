// 基本
import React from "react"
import { injectIntl, FormattedMessage } from 'react-intl';
// Card
import HomePageCard from "../../lib/HomePageCard";
// 主Table
import WorkOrderTable from "./element/work-order-table";
// 新增工作單
import AddWorkOrder from "./element/add-work-order";


class WorkOrderTemplate extends React.Component {

    render(){
        // i18n
        const { formatMessage } = this.props.intl;



        function PageCard() {

            const [value, setValue] = React.useState(0);

            const handleChange = (event, newValue) => {
                setValue(newValue);
            };

            const HomePageCardItems ={
                formatMessage: formatMessage,
                title: formatMessage({
                    id: "home.work_order",
                    defaultMessage: "Work Order"
                }),
                pageItems:[
                    {
                        pageTitle: formatMessage({
                            id: "home.work_order_table",
                            defaultMessage: "Work Order Table"
                        }),
                        pageContent: <WorkOrderTable formatMessage={formatMessage} />,

                    },
                    {
                        pageTitle: formatMessage({
                            id: "home.gantt",
                            defaultMessage: "Gantt"
                        }),
                        pageContent: "Gantt",

                    },
                    {
                        pageTitle: formatMessage({
                            id: "home.add-work-order",
                            defaultMessage: "Gantt"
                        }),
                        pageContent: <AddWorkOrder formatMessage={formatMessage} setValue={ setValue }/>,

                    },
                ]
            }

            return(
                <HomePageCard
                    value={ value }
                    handleChange={ handleChange }
                    HomePageCardItems={ HomePageCardItems }
                />
            )
        }

        return(
            <div className="work-order">
                <PageCard />
            </div>
        )
    }
}

const WorkOrder = injectIntl( WorkOrderTemplate );

export { WorkOrder }
