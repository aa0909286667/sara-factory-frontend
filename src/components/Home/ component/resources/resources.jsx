// 基本
import React from "react"
import { injectIntl, FormattedMessage } from 'react-intl';
// Card
import HomePageCard from "../../lib/HomePageCard";
// 主Table
import EmployeeTable from "./element/employee-table";
import MachineTable from "./element/machine-table";
// 新增資源
import AddResources from "./element/add-resources"

class ResourcesTemplate extends React.Component {

    render(){
        // i18n
        const { formatMessage } = this.props.intl;

        function PageCard (){

            const [value, setValue] = React.useState(0);

            const handleChange = (event, newValue) => {
                console.log(event)
                setValue(newValue);
            };

            const HomePageCardItems ={
                formatMessage: formatMessage,
                title: formatMessage({
                    id: "home.resources",
                    defaultMessage: "Resources"
                }),
                pageItems:[
                    {
                        pageTitle: formatMessage({
                            id: "home.employee_table",
                            defaultMessage: "Employee Table"
                        }),
                        pageContent: <EmployeeTable formatMessage={formatMessage} />,

                    },
                    {
                        pageTitle: formatMessage({
                            id: "home.machine_table",
                            defaultMessage: "Machine Table"
                        }),
                        pageContent: <MachineTable formatMessage={formatMessage} />,

                    },
                    {
                        pageTitle: formatMessage({
                            id: "home.add_resources",
                            defaultMessage: "Add Resources"
                        }),
                        pageContent: <AddResources formatMessage={formatMessage} setValue={ setValue }/>,

                    },
                ],

            }



            return(
                <HomePageCard
                    value={ value }
                    handleChange={ handleChange }
                    HomePageCardItems={ HomePageCardItems }
                />
            )
        }


        return(
            <div className="resources">
                <PageCard />
            </div>
        )
    }
}

const Resources = injectIntl( ResourcesTemplate );

export { Resources }
