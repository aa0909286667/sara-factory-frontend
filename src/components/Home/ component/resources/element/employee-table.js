import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";
import ApiService from "../../../../../services/Home/HomeApi";

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

// Table Key
function createData(employee_id, job_name, multitasking, calendar, working_hours,lock_time) {
    return { employee_id, job_name, multitasking, calendar, working_hours,lock_time };
}

// Table Items
const rows = [];

// 取得製程管理Api
ApiService.getResources().then(
    (response) => {
        const resources = Object.keys(response)
        resources.forEach((name) => {
            if (response[name].resource_type === "operator"){
                rows.push(
                    createData(
                        response[name].resource_name,
                        response[name].job_name.toString(),
                        response[name].allow_overlapped_capacity.toString(),
                        response[name].workingdays,
                        response[name].workinghours.toString().replace(/,/g, ":"),
                        response[name].lock_time.toString() + '小時',
                    )
                )
            }

        })
    }
);

const EmployeeTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'employee_id',
            label:
                formatMessage({
                    id:"resources.employee.employee_id",
                    defaultMessage: "Employee ID"
                }),
        },
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"resources.employee.job_name",
                    defaultMessage: "Job Name"
                }),
        },
        {
            id: 'multitasking',
            label:
                formatMessage({
                    id:"resources.employee.multitasking",
                    defaultMessage: "Multitasking"
                }),
        },
        {
            id: 'calendar',
            label:
                formatMessage({
                    id:"resources.employee.calendar",
                    defaultMessage: "Calendar"
                }),
        },
        {
            id: 'working_hours',
            label:
                formatMessage({
                    id:"resources.employee.working_hours",
                    defaultMessage: "Working Hours"
                }),
        },
        {
            id: 'lock_time',
            label:
                formatMessage({
                    id:"resources.employee.lock_time",
                    defaultMessage: "Lock Time"
                }),
        },
    ];

    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const EmployeeTable = injectIntl( EmployeeTableTemplate );
export default EmployeeTable;
