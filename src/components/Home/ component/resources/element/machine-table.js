import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";
// APi
import ApiService from "../../../../../services/Home/HomeApi";

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

// Table Key
function createData(machine_id, job_name, multitasking, calendar, working_hours, lock_time) {
    return { machine_id, job_name, multitasking, calendar, working_hours,lock_time };
}

// Table Items
const rows = [];

// 取得製程管理Api
ApiService.getResources().then(
    (response) => {
        const resources = Object.keys(response)
        resources.forEach((name) => {
            if (response[name].resource_type === "machine"){
                rows.push(
                    createData(
                        response[name].resource_name,
                        response[name].job_name.toString(),
                        response[name].allow_overlapped_capacity.toString(),
                        response[name].workingdays,
                        response[name].workinghours.toString().replace(/,/g, ":"),
                        response[name].lock_time.toString() + '小時',
                    )
                )
            }

        })
    }
);

const MachineTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'machine_id',
            label:
                formatMessage({
                    id:"resources.machine.machine_id",
                    defaultMessage: "Machine ID"
                }),
        },
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"resources.machine.job_name",
                    defaultMessage: "Job Name"
                }),
        },
        {
            id: 'multitasking',
            label:
                formatMessage({
                    id:"resources.machine.multitasking",
                    defaultMessage: "Multitasking"
                }),
        },
        {
            id: 'calendar',
            label:
                formatMessage({
                    id:"resources.machine.calendar",
                    defaultMessage: "Calendar"
                }),
        },
        {
            id: 'working_hours',
            label:
                formatMessage({
                    id:"resources.machine.working_hours",
                    defaultMessage: "Working Hours"
                }),
        },
        {
            id: 'lock_time',
            label:
                formatMessage({
                    id:"resources.employee.lock_time",
                    defaultMessage: "Lock Time"
                }),
        },
    ];

    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const MachineTable = injectIntl( MachineTableTemplate );
export default MachineTable;
