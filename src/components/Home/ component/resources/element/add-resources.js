// 基本
import React, { useState, useEffect } from "react";
import { injectIntl } from "react-intl";

// 流程ui
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
//select
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
// Text
import TextField from '@material-ui/core/TextField';
// TransferList
import TransferList from "../../../../TransferList/TransferList";
// Api
import ApiService from "../../../../../services/Home/HomeApi";


// Dashboard Info Api

let dashboardInfoList = []

ApiService.getDashboardInfo().then(
    (response) => {
        dashboardInfoList.push(Object.keys(response))
    }
);

// calendar name Api

let calendarNameList = []

ApiService.getCalendar().then(
    (response) => {
        Object.keys(response).forEach((calendar) => {
            calendarNameList.push(response[calendar].calendar_name)
        })
    }

);

// Hours Name Api
let hoursNameList = []

ApiService.getHours().then(
    (response) => {
        Object.keys(response).forEach((calendar) => {
            hoursNameList.push({
                hours_name: response[calendar].hours_name,
                working_hours: response[calendar].working_hours
            })
        })
    }
);


const AddResourcesTemplate = props => {
    // 父頁面值
    const { formatMessage, setValue } = props;

    const [activeStep, setActiveStep] = React.useState(0);

    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
        ApiService.addResource(addResources).then(
            (response) => {
                if (addResources.resource_type === "operator") {
                    setValue(0)
                } else if (addResources.resource_type === "machine") {
                    setValue(1)
                }
            }
        );
    };


    // 資源類型
    const [addResources, setAddResources] = useState({
        resource_type: 'operator', // 資源類型
        resource_name: '', // 編號
        job_name: [], //製程能力
        allow_overlapped: 1, // 多工作業
        calendar_name: calendarNameList[0], // 行事曆
        hours_name: hoursNameList[0].hours_name, // 工作時段

    })

    const [open, setOpen] = React.useState(false);


    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const [allowOverlappedOpen, setAllowOverlappedOpen] = React.useState(false);


    const allowOverlappedCloseSelect = () => {
        setAllowOverlappedOpen(false);
    };

    const allowOverlappedOpenSelect = () => {
        setAllowOverlappedOpen(true);
    };

    const [jobName, setJobName] = React.useState([]);

    useEffect(() => {
        setAddResources({...addResources,job_name: jobName })
    },[jobName])


    function getSteps() {
        return ['選擇資源類型', "輸入編號", '選擇製程能力（技能）',"選擇多工作業","選擇行事曆","選擇工作時段"];
    }



    function getStepContent(step) {
        switch (step) {
            case 0:
                return (
                    <div>
                        <h3 className="margin-20 f-16">請選擇資源類型</h3>
                        <Select
                            className="margin-20"
                            open={ open }
                            value={ addResources.resource_type }
                            onClose={ handleClose }
                            onOpen={ handleOpen }
                            onChange={(evt) => {
                                setAddResources({...addResources,resource_type: evt.target.value });
                            }}
                            style={{ minWidth: 120, }}
                        >
                            <MenuItem value={ "operator" }>人員</MenuItem>
                            <MenuItem value={ "machine" }>機台</MenuItem>
                            <MenuItem value={ "vendor" }>外包</MenuItem>
                        </Select>
                    </div>
                );
            case 1:
                return (
                    <div>
                        <h3 className="margin-20 f-16">請輸入編號</h3>
                        <TextField
                            className="margin-20"
                            type="string"
                            id="standard-multiline-flexible"
                            multiline
                            value={ addResources.resource_name }
                            onChange={(evt) => {
                                setAddResources({...addResources,resource_name: evt.target.value });
                            }}
                            rowsMax={4}
                        />
                    </div>
                );
            case 2:
                return (
                    <TransferList leftList={ dashboardInfoList[0] } rightList={ jobName } setRightList={ setJobName }/>
                );
            case 3:
                return (
                    <div>
                        <h3 className="margin-20 f-16">請選擇多工作業</h3>
                        <Select
                            className="margin-20"
                            open={ allowOverlappedOpen }
                            value={ addResources.allow_overlapped }
                            onClose={ allowOverlappedCloseSelect }
                            onOpen={ allowOverlappedOpenSelect }
                            onChange={(evt) => {
                                setAddResources({...addResources,allow_overlapped: evt.target.value });
                            }}
                            style={{ minWidth: 120, }}
                        >
                            <MenuItem value={ 1 }> { 1 } </MenuItem>
                            <MenuItem value={ 2 }> { 2 } </MenuItem>
                            <MenuItem value={ 3 }> { 3 } </MenuItem>
                            <MenuItem value={ 4 }> { 4 } </MenuItem>
                            <MenuItem value={ 5 }> { 5 } </MenuItem>
                            <MenuItem value={ 6 }> { 6 } </MenuItem>
                            <MenuItem value={ 7 }> { 7 } </MenuItem>
                            <MenuItem value={ 8 }> { 8 } </MenuItem>
                            <MenuItem value={ 9 }> { 9 } </MenuItem>
                            <MenuItem value={ 10 }> { 10 } </MenuItem>
                            <MenuItem value={ 11 }> { 11 } </MenuItem>
                            <MenuItem value={ 12 }> { 12 } </MenuItem>
                            <MenuItem value={ 13 }> { 13 } </MenuItem>
                            <MenuItem value={ 14 }> { 14 } </MenuItem>
                            <MenuItem value={ 15 }> { 15 } </MenuItem>
                            <MenuItem value={ 16 }> { 16 } </MenuItem>
                            <MenuItem value={ 17 }> { 17 } </MenuItem>
                            <MenuItem value={ 18 }> { 18 } </MenuItem>
                            <MenuItem value={ 19 }> { 19 } </MenuItem>
                            <MenuItem value={ 20 }> { 20 } </MenuItem>
                            <MenuItem value={ 21 }> { 21 } </MenuItem>
                            <MenuItem value={ 22 }> { 22 } </MenuItem>
                            <MenuItem value={ 23 }> { 23 } </MenuItem>
                            <MenuItem value={ 24 }> { 24 } </MenuItem>
                            <MenuItem value={ 25 }> { '無限產能' } </MenuItem>
                        </Select>
                    </div>
                );
            case 4:
                return (
                    <Select
                        className="margin-20"
                        open={ open }
                        value={ addResources.calendar_name }
                        onClose={ handleClose }
                        onOpen={ handleOpen }
                        onChange={(evt) => {
                            setAddResources({...addResources,calendar_name: evt.target.value });
                        }}
                        style={{ minWidth: 120, }}
                    >
                        {
                            calendarNameList.map((calendarName) => {
                                return(
                                    <MenuItem key={calendarName} value={ calendarName }>{ calendarName }</MenuItem>
                                )
                            })
                        }

                    </Select>
                );
            case 5:
                return (
                    <Select
                        className="margin-20"
                        open={ open }
                        value={ addResources.hours_name }
                        onClose={ handleClose }
                        onOpen={ handleOpen }
                        onChange={(evt) => {
                            setAddResources({...addResources,hours_name: evt.target.value });
                        }}
                        style={{ minWidth: 120, }}
                    >
                        {
                            hoursNameList.map((hoursName) => {
                                return(
                                    <MenuItem
                                        key={ hoursName.working_hours }
                                        value={ hoursName.hours_name }
                                    >{ hoursName.hours_name + ': ' +  hoursName.working_hours.toString() }
                                    </MenuItem>
                                )
                            })
                        }

                    </Select>
                );
            default:
                return 'Unknown step';
        }
    }

    return(
        <div style={{ maxHeight: "426px",  height: "426px" }} className="overflow-y-auto">
            <Stepper activeStep={activeStep} orientation="vertical">
                {steps.map((label, index) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                        <StepContent>
                            <Typography component="span" >{getStepContent(index)}</Typography>
                            <div>
                                <div>
                                    <Button
                                        className="margin-20"
                                        color="secondary"
                                        variant="contained"
                                        disabled={activeStep === 0}
                                        onClick={ handleBack }
                                    >
                                        Back
                                    </Button>
                                    <Button
                                        className="margin-20"
                                        disabled={
                                            (activeStep === 1 && addResources.resource_name === "") ||
                                            (activeStep === 2 && jobName.length <= 0)

                                        }
                                        variant="contained"
                                        color="primary"
                                        onClick={ handleNext }
                                    >
                                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                    </Button>
                                </div>
                            </div>
                        </StepContent>
                    </Step>
                ))}
            </Stepper>
            {activeStep === steps.length && (
                <Paper square elevation={0}>
                    <Typography className="margin-20"  component="span">所有步骤完成</Typography>
                    <Button
                        color="primary"
                        onClick={ handleReset }
                        variant="contained"
                        className="margin-20"
                    >
                        送出
                    </Button>
                </Paper>
            )}
        </div>

    )
}

const AddResources = injectIntl( AddResourcesTemplate );
export default AddResources;
